import axios from 'axios'
let generatedIds = [1]

export async function fetchWord () {
    const min = 2;
    const max = 1368;
    const generatedUid = Math.floor(Math.random() * (max - min + 1)) + min;
    let {data} = await axios.get(`https://apidir.pfdo.ru/v1/directory-program-activities/${generatedUid}`)
    if (typeof data.data === 'object' && data.data.status === 10 && (!generatedIds.includes(generatedUid))) {
        generatedIds.push(generatedUid)
        return data.data
    }
    return fetchWord()
}
